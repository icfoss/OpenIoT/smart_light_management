/*
 * Description:  Control LED with AskSensors and ESP32 dev board over MQTT
 *  Author: https://asksensors.com, 2020
 *  github: https://github.com/asksensors
 */
 
 
#include <WiFi.h>
#include <PubSubClient.h>

//TODO: ESP32 MQTT user config
//const char* ssid = "Redmi"; // Wifi SSID
//const char* password = "12345678";
//const char* ssid = "RealmeC"; // Wifi SSID
//const char* password = "anoojask";
const char* ssid = "ICFOSS"; // Wifi SSID
const char* password = "n0tava!lableG32!"; // Wifi Password
const char* username = "anooja"; // my AskSensors username
const char* subTopic = "light_2"; // actuator/username/apiKeyOut
const int LED_pin = 22; // LEd pin


 unsigned long currentMillis1;
//AskSensors MQTT config
const char* mqtt_server = "mqtt.icfoss.org";
unsigned int mqtt_port = 1883;
unsigned long interval1=3000;
unsigned long previousMillis1 = 0;

void WiFiretake();
WiFiClient espClient;
PubSubClient client(espClient);

void setup() {
  Serial.begin(115200);
  Serial.println("*****************************************************");
  Serial.println("********** Program Start : ESP32 controls LED with AskSensors over MQTT");
  Serial.println("Set LED as output");
  
  pinMode(LED_pin, OUTPUT);   // set led as output
  
  Serial.print("********** connecting to WIFI : ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    Serial.println("wifi first connection");
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("->WiFi connected");
  Serial.println("->IP address: ");
  Serial.println(WiFi.localIP());
  
  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);

  if (!client.connected()) 
    reconnect();
  Serial.print("********** Subscribe to AskSensors actuator topic:");
  Serial.print(subTopic);
  // susbscribe
  client.subscribe(subTopic);
  
}

void loop() {

   client.loop();
   reconnect();
  }
  
   

String s;
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Command Received from mqtt icfoss[");
  Serial.print(topic);
  Serial.println("] ");
  int i = 0;
  Serial.println("inside the callback");

  for (i = 0; i < length; i++) {
    // Serial.print("i for loop");
    Serial.print((char)payload[i]); 
  }
  payload[i]='\0';
  Serial.println("");
  Serial.println("********** Parse Actuator command");  
  Serial.print("$$$");
  Serial.println((char *)payload);
  Serial.print((char*)"module1=1");
  Serial.println("$$$");
  
  if(strcmp((char *)payload,(char*)"module1=1")==0){ 
    Serial.println("string compair ");
    digitalWrite(LED_pin, HIGH);
    Serial.println("LED is ON");   
  } 
  else{
    digitalWrite(LED_pin, LOW);
    Serial.println("LED is OFF");
   }
}



void reconnect() {

 while(!client.connected()) {
  WiFiretake();
  Serial.print("**********Attempting MQTT connection...");
  // Attempt to connect
    if (client.connect("espClient", username, "manumohan")) {  
     Serial.println("-> MQTT client connected");
     
   } else {
    Serial.print("failed, rc=");
    Serial.print(client.state());
    Serial.println("-> try again in 5 seconds");
    delay(5000);// Wait 5 seconds before retrying
     
  }
  
   
 client.subscribe(subTopic);
   
 }
  
  }

void WiFiretake(){
  unsigned long currentMillis1 = millis();
  // if WiFi is down, try reconnecting every CHECK_WIFI_TIME seconds
  if ((WiFi.status() != WL_CONNECTED) && (currentMillis1 - previousMillis1 >=interval1)) {
    //Serial.print(millis());
    Serial.println("Reconnecting to WiFi...");
    WiFi.disconnect();
    Serial.println("wifi is struggling");
    WiFi.reconnect();
    delay(5000);
    Serial.println("wifi is rejoin");
    previousMillis1 = currentMillis1;
    Serial.println("initWifi previousMillis = currentMillis;");
  }
  }
